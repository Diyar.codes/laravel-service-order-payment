<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function index(Request $request) {
        $userId = $request->query('user_id');

        $orders = Order::query();
        $orders->when($userId, function($query) use ($userId) {
            return $query->where('user_id', '=', $userId);
        });
        $orders = $orders->get();

        return $this->ResponeSuccess('success get all payment', $orders);
    }

    public function store(Request $request) {
        try {
            DB::beginTransaction();
            $order = Order::create([
                'user_id' => $request->user['id'],
                'course_id'  => $request->course['id']
            ]);

            $transactionDetails = [
                'order_id'     => $order->id . '-' . Str::random(5),
                'gross_amount' => $request->course['price']
            ];

            $itemDetails = [
                [
                    'id'       => $request->course['id'],
                    'price'    => $request->course['price'],
                    'quantity' => 1,
                    'name'     => $request->course['name'],
                    'brand'    => 'BuildWithAngga',
                    'category' => 'Online Course'
                ]
            ];

            $customerDetails = [
                'first_name' => $request->user['name'],
                'email'      => $request->user['email']
            ];

            $midtransParams = [
                'transaction_details' => $transactionDetails,
                'item_details'        => $itemDetails,
                'customer_details'    => $customerDetails
            ];

            $midtransSnapUrl = $this->getMidtransSnapUrl($midtransParams);

            $order->snap_url = $midtransSnapUrl;
            $order->metadata = [
                'course_id'        => $request->course['id'],
                'course_price'     => $request->course['price'],
                'course_name'      => $request->course['name'],
                'course_thumbnail' => $request->course['thumbnail'],
                'course_level'     => $request->course['level']
            ];
            $order->save();

            return $this->ResponeSuccessStore('success store payment', $order);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    private function getMidtransSnapUrl($params) {
        \Midtrans\Config::$serverKey = env('MIDTRANS_SERVER_KEY');
        \Midtrans\Config::$isProduction = (bool) env('MIDTRANS_PRODUCTION');
        \Midtrans\Config::$is3ds = (bool) env('MIDTRANS_3DS');

        $snapUrl = \Midtrans\Snap::createTransaction($params)->redirect_url;
        return $snapUrl;
    }
}
