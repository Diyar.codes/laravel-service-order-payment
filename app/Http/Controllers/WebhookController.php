<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\PaymentLog;
use Illuminate\Http\Request;

class WebhookController extends Controller
{
    public function midtransHandler(Request $request) {
        $signatureKey = $request->signature_key;

        $orderId = $request->order_id;
        $statusCode = $request->status_code;
        $grossAmount = $request->gross_amount;
        $serverKey = env('MIDTRANS_SERVER_KEY');

        $mySignatureKey = hash('sha512', $orderId.$statusCode.$grossAmount.$serverKey);

        $transactionStatus = $request->transaction_status;
        $type = $request->payment_type;
        $fraudStatus = $request->fraud_status;

        if($signatureKey !== $mySignatureKey) {
            return $this->ValidationError('invalid signature');
        }

        $realOrderId = explode('-', $orderId);

        $order = Order::find($realOrderId[0]);
        if(!$order) {
            return $this->ValidationErrorNotFound('order id not found');
        }

        if($order->status === 'success') {
            return $this->ValidationErrorPermitted('operation not permitted');
        }

        if($transactionStatus == 'capture') {
            if($fraudStatus == 'challenge') {
                $order->status = 'challenge';
            } else if($fraudStatus == 'accept') {
                $order->status = 'success';
            }
        } else if($transactionStatus == 'settlement') {
            $order->status = 'success';
        } else if($transactionStatus == 'cancel' || $transactionStatus == 'deny' || $transactionStatus == 'expire') {
            $order->status = 'failure';
        } else if($transactionStatus == 'pending') {
            $order->status = 'pending';
        }

        $historyData = [
            'status'       => $transactionStatus,
            'raw_response' => json_encode($request->all()),
            'order_id'     => $realOrderId[0],
            'payment_type' => $type
        ];

        PaymentLog::create($historyData);
        $order->save();

        if($order->status === 'success') {
            createPremiumAccess([
                'user_id'   => $order->user_id,
                'course_id' => $order->course_id
            ]);
        }

        return response()->json('OK');
        // return $this->ResponeSuccessStore('success store webhook', $order);
    }
}
