<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ValidationError($validator)
    {
        return response()->json([
            "status"  => 'error',
            'message' =>  $validator,
            'data'    => null,
        ], 400);
    }

    public function ValidationErrorNotFound($validator)
    {
        return response()->json([
            "status"  => 'error',
            'message' =>  $validator,
            'data'    => null,
        ], 404);
    }

    public function ValidationErrorPermitted($validator)
    {
        return response()->json([
            "status"  => 'error',
            'message' =>  $validator,
            'data'    => null,
        ], 405);
    }

    public function ResponeSuccessStore($message, $data)
    {
        return response()->json([
            'status'  => 'success',
            'message' => $message,
            'data'    => $data
        ], 201);
    }

    public function ResponeSuccess($message, $data)
    {
        return response()->json([
            'status'  => 'success',
            'message' => $message,
            'data'    => $data
        ], 200);
    }

    public function ServerError($message)
    {
        return response()->json([
            "status"  => 'error',
            'message' =>  $message,
            'data'    => null,
        ], 400);
    }
}
