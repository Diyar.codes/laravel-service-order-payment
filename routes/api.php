<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\WebhookController;

// mentor
Route::namespace('api')->prefix('v1/orders')->group(function () {
    Route::get('/', [OrderController::class, 'index']);
    Route::post('/', [OrderController::class, 'store']);
});

// webhook
Route::namespace('api')->prefix('v1/webhook')->group(function () {
    Route::post('/', [WebhookController::class, 'midtransHandler']);
});
